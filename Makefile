SHELL := /bin/bash
USER_ID ?= `id -u`
GROUP_ID ?= `id -g`

HOST ?= localhost
ifeq ($(HOST), localhost)
	PORT ?= 5555
else
	PORT ?= 22
endif

PRIV_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))
PUBLIC_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))

out/gateway.iso: archiso/airootfs/etc/systemd/system/infra.service
	@umask 0022
	@sudo rm out/ tmp/ -rf || /bin/true
	@mkdir out
	@cp archiso/packages.x86_64.base archiso/packages.x86_64
	@tr " " "\n" <<< "$(EXTRA_PACKAGES)" >> archiso/packages.x86_64
	ssh-keygen -f out/gateway_key -P ""
	@cp out/gateway_key.pub archiso/airootfs/root/.ssh/authorized_keys
	@chmod 600 archiso/airootfs/root/.ssh/authorized_keys
	sudo --preserve-env="FAST_COMPRESSION,EXTRA_PACKAGES" mkarchiso -v -w tmp -o out archiso/
	@sudo chown -R $(USER_ID):$(GROUP_ID) out
	@sudo rm -r tmp archiso/packages.x86_64
	@sudo mv out/*.iso out/gateway.iso

build: out/gateway.iso

use_docker:
	@if test "$(CONTAINER)" = "" ; then \
		echo "Usage: make use_docker CONTAINER=registry.freedesktop.org/gfx-ci/radv-infra:latest"; \
		exit 1; \
	fi
	@sed "s~{CONTAINER}~${CONTAINER}~g" templates/docker.service > archiso/airootfs/etc/systemd/system/infra.service
	@echo "Configuration done: use 'make build' to generate the image"

use_web:
	@if test "$(URL)" = "" ; then \
		echo "Usage: make use_web URL=https://example.com/script.sh"; \
		exit 1; \
	fi
	@sed "s|{URL}|${URL}|g" templates/web.service > archiso/airootfs/etc/systemd/system/infra.service
	@echo "Configuration done: use 'make build' to generate the image"

use_local:
	@if test "$(APP)" = "" ; then \
		echo "Usage: make use_local APP=/path/to/script/folder"; \
		exit 1; \
	fi
	@cp templates/local.service archiso/airootfs/etc/systemd/system/infra.service
	@rsync -av --delete ${APP}/ archiso/airootfs/app/
	@echo "Configuration done: use 'make build' to generate the image"

run: out/gateway.iso
	[ -f out/disk.img ] || fallocate -l 5G out/disk.img
	@ip link show bridge_priv 2> /dev/null || ( \
		echo "Setting up the bridge. This requires root rights!"; \
		sudo ip link add bridge_priv type bridge; \
		sudo ip link set bridge_priv up; \
		sudo iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT; \
	)
	qemu-system-x86_64 -m size=4096 -k en -device virtio-scsi-pci,id=scsi0 -device scsi-cd,bus=scsi0.0,drive=cdrom0 -drive id=cdrom0,if=none,format=raw,media=cdrom,readonly=on,file=out/gateway.iso -display gtk -vga std -device virtio-net-pci,romfile=,netdev=net0,mac=$(PUBLIC_MAC) -netdev user,id=net0,hostfwd=tcp::5555-:22 -net nic,macaddr=$(PRIV_MAC) -net bridge,br=bridge_priv -drive file=out/disk.img,format=raw,index=0,media=disk -enable-kvm -serial stdio -boot d -no-reboot

start_netboot_dut:
	qemu-system-x86_64 -m size=1024 -net nic,macaddr=$(PRIV_MAC) -net bridge,br=bridge_priv -boot n

test:
	@ssh -i out/gateway_key root@localhost -p 5555 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null /usr/bin/selftest

connect:
	@ssh -i out/gateway_key root@${HOST} -p ${PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

clean:
	rm -f archiso/packages.x86_64
	rm -f archiso/airootfs/etc/systemd/system/infra.service
	rm -rf archiso/airootfs/app
	git checkout archiso/airootfs/app/entrypoint
	sudo rm -rf out tmp
	rm -f archiso/airootfs/root/.ssh/authorized_keys
