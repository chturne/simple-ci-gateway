#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="simple-ci-gateway"
iso_label="SIMPLE_CI_GATEWAY_$(date +%Y%m)"
iso_publisher="Arch Linux <https://www.archlinux.org>"
iso_application="Simple CI Gateway Image"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito')  # 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito'
arch="x86_64"
pacman_conf="pacman.conf"

file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/usr/bin/disk_setup"]="0:0:755"
  ["/usr/bin/find_nic"]="0:0:755"
  ["/usr/bin/network_setup"]="0:0:755"
  ["/usr/bin/selftest"]="0:0:755"
  ["/app/entrypoint"]="0:0:755"
)

if [ -z ${FAST_COMPRESSION+x} ]; then
    airootfs_image_tool_options=('-comp' 'xz')
else
    echo "WARNING: Suboptimal compression level"
    airootfs_image_tool_options=('-comp' 'gzip')
fi
